
'use strict';

import React from 'react';

//Windows interface.
//import ReactUWP from 'react-uwp';

//Android interface.

//iOS interface.

//Web interface.
// import PureCSS from 'pure-css';

 class MyAwesomeApp extends React.Component {  
  render() {
    return(
      <html lang="en">
      <head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
        <link rel="stylesheet" href="https://miguhruiz.xyz/public/css/index.css" />
        <link rel="manifest" href="./public/manifest.json" />
        <title>My awesome app rendered at the server</title>
      </head>
      <body>
// El lugar donde el build colocará los componentes.
        <div className="react-app"></div>
// El archivo con la lógica de nuestros componentes, en este caso el header.
      </body>
      </html>
    )
  }
}

export default MyAwesomeApp;
