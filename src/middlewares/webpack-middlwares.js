"use strict";

export default (app, webpackDevMiddleware, webpack, webpackConfig) => {
    app.use(webpackDevMiddleware(webpack(webpackConfig)));
}