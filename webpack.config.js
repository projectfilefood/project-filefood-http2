import webpack from 'webpack';
import path from 'path';


export default{
    mode : 'production',

    entry: {
    client: "./src/client/index.js",
    bundle: "./src/client/bundle.js",
  },

  output: {
    path: path.resolve(__dirname, 'assets'),
    filename: "[name].js",
  },

  module: {
        rules: [
            {
                use : 'babel-loader',
                test :/\.js$/,
                exclude : /node_modules/
            },

            {
                use: ['style-loader', 'css-loader'],
                test: /\.css$/
            },

            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader', options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader', options: {
                            sourceMap: true
                        }
                    }
                ]
            },

            {
                test: /\.(pdf|jpg|png|gif|svg|ico|json)$/,
                use: [
                    {
                        loader: 'url-loader'
                    },
                ]
            },
        ]
    },
}