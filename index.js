
"use strict";

import express from 'express';
import helmet from 'helmet';
import { readFileSync } from 'fs';
import { createServer } from 'spdy';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackConfig from './webpack.config';

const options = {
key: readFileSync("./key.pem"),
cert: readFileSync("./certificate.pem"),
spdy: {
    protocols: [ 'h2','https' ],
    plain: false,
 
    connection: {
      windowSize: 1024 * 1024,
 
      autoSpdy31: false
    }
}
};

const PORT = 4433;
const app = express();

app.use(helmet()); 
require('./src/middlewares/webpack-middlwares').default(app, webpackDevMiddleware, webpack, webpackConfig);

require('./src/app/routes.js').default(app);

spdy.createServer(app, options).listen(PORT, err => {

    if (err) {
        console.error(err);
    } {
        console.log(`App Listening on port: ${PORT}`);
    }

});


